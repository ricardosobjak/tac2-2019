const express = require('express');
const router = express.Router();
const userRoutes = require('./user');
const isAuthorized = require('../middlewares/auth')

router.get('/', (request, response) => {
    //response.send("Olá usuário!");

    //response.setHeader('content-type', 'application/json');
    //response.send(JSON.stringify({ message: "Olá" }));

    response.json({ message: "Oi denovo" });
});

router.use('/user', [isAuthorized], userRoutes);

module.exports = router;