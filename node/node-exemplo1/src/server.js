const express = require('express');
const mongoose = require('mongoose');

const app = express();
const routes = require('./routes/routes');


mongoose.connect('mongodb://localhost/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true})


// Reconhecer o body da requisição como JSON
app.use(express.json()); 

app.use(routes);

app.listen(3000);